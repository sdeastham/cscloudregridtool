# README #

### Quick summary ###

CSCloudRegridTool regrids cloud variables in either direction between rectlinear latitude-longitude files and cubed-sphere files. However, it is intended exclusively to regrid a small subset of cloud-relevant variables using albedo weighting.

### Setup, compilation and execution ###

For more information on building and using the tool, see the README file for the standard CSRegridTool (https://bitbucket.org/sdeastham/csregridtool).

### Who do I talk to? ###

For further assistance, please contact seastham@mit.edu.